/* eslint-disable camelcase */
/* eslint-disable no-return-await */
import { BASE_URL } from '@configs/constants';
import axios, {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios';

export const HttpClient = axios.create({
  timeout: 30000,
  headers: {
    'Cache-Control': 'no-store',
    responseType: 'application/json',
  },
  baseURL: BASE_URL,
});

const onRequest = (config: any): AxiosRequestConfig => {
  return config;
};

const onRequestError = (error: AxiosError): Promise<AxiosError> =>
  // console.error(`[request error] [${JSON.stringify(error)}]`);
  Promise.reject(error);
const onResponse = (response: AxiosResponse): AxiosResponse =>
  // console.info(`[response] [${JSON.stringify(response)}]`);

  response;
const onResponseError = (error: AxiosError): Promise<AxiosError> => {
  return Promise.reject(error);
};

export const setupInterceptorsTo = (
  axiosInstance: AxiosInstance,
): AxiosInstance => {
  axiosInstance.interceptors.request.use(onRequest, onRequestError);
  axiosInstance.interceptors.response.use(onResponse, onResponseError);
  return axiosInstance;
};

setupInterceptorsTo(HttpClient);

export interface IAPIResponse<T = any> {
  data: T;
  message: string;
  status: 200 | 400 | 300 | 500;
  payload: any;
}

export const getData = async (url: string, config = {}) =>
  await HttpClient.get<IAPIResponse>(url, { ...config }).then(
    (response) => response,
  );

export const postData = async (url: string, data = {}) =>
  await HttpClient.post<IAPIResponse>(url, { ...data }).then(
    (response) => response,
  );

export const patchData = async (url: string, data = {}) =>
  await HttpClient.patch<IAPIResponse>(url, { ...data }).then(
    (response) => response,
  );

export const deleteData = async (url: string, data = {}) =>
  await HttpClient.delete<IAPIResponse>(url, { ...data }).then(
    (response) => response,
  );

export const putData = async (url: string, data = {}) =>
  await HttpClient.put<IAPIResponse>(url, { ...data }).then(
    (response) => response,
  );
