import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { isReadyRef } from 'react-navigation-helpers';
import { UIManager, Platform, LayoutAnimation } from 'react-native';
import RootNavigator from '@navigation/rootNavigator';

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
  LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
}

function Navigation() {
  React.useEffect((): any => {
    // eslint-disable-next-line no-return-assign
    return () => (isReadyRef.current = false);
  }, []);

  return (
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  );
}

export default Navigation;
