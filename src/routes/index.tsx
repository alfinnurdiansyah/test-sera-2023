import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

/**
    prefix penamaan pake nama module contoh => NamaModulesNamaScreen = AuthLogin
*/

enum ROUTERS {
  HomeMain = 'HomeMain',
  HomeDetailMovie = 'HomeDetailMovie',

}

export type RootStactNavigationTypes = {
  [ROUTERS.HomeMain]: undefined;
  [ROUTERS.HomeDetailMovie]: undefined;
  // end ticket type
};

const Stack = createNativeStackNavigator<RootStactNavigationTypes>();
const { Navigator } = Stack;
const { Screen } = Stack;

export { Stack, NavigationContainer, Navigator, Screen, ROUTERS };
