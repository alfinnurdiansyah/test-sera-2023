export type IPayloadGetListMovie = {
  isFetch: boolean;
};
export type IPayloadGetDetailMovie = {
  isFetch: boolean;
  movieId: string;
};
export interface IPayloadAddFavorite {
  id: string;
}
export interface IPayloadRemoveFavorite {
  id: string;
}