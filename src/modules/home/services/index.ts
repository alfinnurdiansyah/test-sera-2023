import { getData } from '@utils/httpClient';
import { Key } from '@configs/constants';
import { IPayloadGetDetailMovie } from '../types';
export const homeGetListMovie = () =>
  getData(
    `movie/popular?api_key=`+ Key,
  );
export const homeGetDetailMovie = (params: IPayloadGetDetailMovie) =>
  getData(
    `movie/`+ params?.movieId +`?api_key=`+ Key,
  );

