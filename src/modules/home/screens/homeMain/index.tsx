import React, { useCallback, useMemo } from "react";
import {
  SafeAreaView,
  View,
} from "react-native";
import { useIsFocused, useNavigation } from "@react-navigation/native";
import { BASE_URL_IMAGE } from '@configs/constants';
import { Box, Heading, Pressable, Image, Text, Stack, NativeBaseProvider } from "native-base";
import styles from "./styles";
import { FlashList } from "@shopify/flash-list";
import { useGetListMovie } from "@modules/home/hooks";
import { ROUTERS } from "@routes/index";

const Component = () => {
  useIsFocused();
  const navigation = useNavigation();
  const {data: dataListMovie} =
    useGetListMovie({
      isFetch: true
    });
  const listMovie = useMemo(
    () => dataListMovie?.data?.results ?? [],
    [dataListMovie?.data?.results],
  );
  const _handleGetDetailMovie = useCallback((id:string,title:string) => {
    navigation.navigate(ROUTERS.HomeDetailMovie,{id,name:title})
  }, []);
  const _renderItem = ({ item, index }: any) => (
      <Pressable onPress={() => _handleGetDetailMovie(item.id,item.title)}>
        <Box bg="white" shadow={2} rounded="lg" maxWidth="95%" alignSelf="center" marginTop={5}>
          <Image source={{
          uri: BASE_URL_IMAGE+item.backdrop_path
        }} alt="image base" resizeMode="cover" height={150} roundedTop="md" />
          <Text bold position="absolute" color="white" top={0} m={[4, 4, 8]}>
            MOVIE
          </Text>
          <Stack space={2} p={[4, 4, 8]}>
            <Text color="gray.400">Release Date : {item.release_date}</Text>
            <Heading noOfLines={2}>
              {item.title}
            </Heading>
            <Text lineHeight={[14, 14, 14]} numberOfLines={3} color="gray.700">
              {item.overview}
            </Text>
          </Stack>
        </Box>
      </Pressable>
  );
  return (
    <NativeBaseProvider>
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <View style={styles.container}>
            <Box maxWidth="95%" alignSelf="center" marginTop={5}>
              <Heading color="gray.700">
                List Movie
              </Heading >
            </Box>
            <FlashList
              keyboardShouldPersistTaps="handled"
              data={listMovie}
              keyExtractor={(_item, index) => index.toString()}
              renderItem={_renderItem}
            />
          </View>
        </View>
      </SafeAreaView>
    </NativeBaseProvider>
  );
};

export default Component;