import React, { useCallback, useMemo } from "react";
import {
  Pressable,
  SafeAreaView,
  View,
  TouchableOpacity
} from "react-native";
import { useDispatch } from "react-redux";
import { useIsFocused } from "@react-navigation/native";
import { BASE_URL_IMAGE } from '@configs/constants';
import { Box, Heading, Image, Text, Stack, NativeBaseProvider,FlatList, FavouriteIcon } from "native-base";
import styles from "./styles";
import { useGetDetailMovie } from "@modules/home/hooks";
import { actions as actionsHome } from '@modules/home/stores/index';
import { useAppSelector } from "@hook/useStore";
import { findIndex } from "lodash";

declare const global: { HermesInternal: null | {} };
interface IProps {
  navigation?: any;
  route?: any;
}

const Component = (props: IProps) => {
  useIsFocused();
  const { route } = props;
  const dispatch = useDispatch();
  const {
    listMovie,
  } = useAppSelector((state) => state.home);
  const {
    params: { id: movieId },
  } = route;
  const {data: dataDetailMovie} =
    useGetDetailMovie({
      isFetch: true,
      movieId: movieId ?? '',
    });
  const detailMovie = useMemo(
    () => dataDetailMovie?.data ?? {},
    [dataDetailMovie?.data],
  );
  const _renderItem = ({ item }: any) => (
    <Box alignSelf="center" p={1.5} borderColor={"blue.400"} marginLeft={2} borderWidth={1} borderRadius={16} _text={{
      fontSize: "md",
      fontWeight: "medium",
      letterSpacing: "lg",
      color: "blue.700"
    }}>
      {item.name}
    </Box>
  );
  const _handleClickFavorite = useCallback(() => {
    dispatch(
      actionsHome.HomeFavorite({
        movieId
      }),
    );
  }, [dispatch]);
  const RenderMain = useMemo(() => {
    return (
      <NativeBaseProvider>
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <Box height={300}>
              <Image opacity={80} source={{
                uri: BASE_URL_IMAGE + detailMovie.backdrop_path
              }} alt="image base" resizeMode="cover" height={300} />
              <Box position="absolute" alignSelf={"center"} height={250} justifyContent={"center"}>
                <Image source={{
                  uri: BASE_URL_IMAGE + detailMovie.poster_path
                }} alt="image base" resizeMode="contain" height={200} width={110} />
              </Box>
              <Heading color="white" position="absolute" bottom={2} alignSelf="center">
                {detailMovie.title}
              </Heading >
              <TouchableOpacity onPress={() => _handleClickFavorite()}>
                {findIndex(listMovie, { id: movieId }) === -1 ? <FavouriteIcon size="8" color={'#FFFFFF'} position="absolute" bottom={12} alignSelf="center"/> : <FavouriteIcon size="8" color={'#FF0000'} position="absolute" bottom={12} alignSelf="center"/>}
              </TouchableOpacity>
            </Box>
            <Box p={1} flexDir={"row"} justifyContent="space-around" alignItems={"center"}>
              <Text fontSize="lg" color={"gray.400"}>{detailMovie.runtime} Minutes</Text>
              <Text fontSize="lg" color={"gray.400"}>{detailMovie.release_date}</Text>
              <Box width={10} height={10} borderRadius={40} bgColor="red.400" alignItems={"center"} justifyContent="center">
                <Text bold fontSize="xl" color={"white"}>{detailMovie.vote_average}</Text>
              </Box>
            </Box>
            <Box p={3} flexDir={"row"} alignItems={"center"}>
              <Text bold fontSize="lg">Genre</Text>
              <FlatList
                showsHorizontalScrollIndicator={false}
                horizontal
                keyboardShouldPersistTaps="handled"
                data={detailMovie.genres}
                keyExtractor={(item, index) => index.toString()}
                renderItem={_renderItem}
              />
            </Box>
            <Box p={3}>
              <Text bold fontSize="lg">Overview</Text>
              <Text fontSize="md" color={"gray.400"}>{detailMovie.overview}</Text>
            </Box>
          </View>
        </SafeAreaView>
      </NativeBaseProvider>
    );
  }, [detailMovie, listMovie, movieId]);

  return RenderMain;
};

export default Component;