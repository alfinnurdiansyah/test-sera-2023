import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import initialState from './homeInitialState';
import { findIndex, remove } from 'lodash';

const HomeSlice = createSlice({
  name: 'Home',
  initialState,
  reducers: {
    HomeFavorite: (state, action) => {
      const index = findIndex(state.listMovie, function (o) {
        return o.id === action.payload.movieId;
      });
      if (index === -1) {
        return {
          ...state,
          action: action.type,
          listMovie:[...state.listMovie, {id: action.payload.movieId}]
        };
      } else {
        const newList = remove(state.listMovie, function (n) {
          return n.id === action.payload.movieId;
        });
        return {
          ...state,
          action: action.type,
          listMovie: newList,
        };
        
      }
    },
  },
});

export const {name, actions, reducer} = HomeSlice;
