import { useQuery } from '@tanstack/react-query';
import * as services from '@modules/home/services/index';
import { IPayloadGetDetailMovie } from '../types';

/**
 * dok lengkap dsini https://tanstack.com/query/v4/docs/guides/queries
 * @returns
 */
const useGetDetailMovie = (props: IPayloadGetDetailMovie) => {
  // perlu kasih nama prefix namaModule di depan nya untuk yg sifat nya list karena biar gak bentrok
  const data: any = useQuery(
    ['get-detail-movie-' + props?.movieId],
    () => {
      return services.homeGetDetailMovie(props);
    },
    {
      enabled: props?.isFetch,
    },
  );
  return data;
};

export default useGetDetailMovie;
