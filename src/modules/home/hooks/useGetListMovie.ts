import { useQuery } from '@tanstack/react-query';
import * as services from '@modules/home/services/index';
import { IPayloadGetListMovie } from '../types';

/**
 * dok lengkap dsini https://tanstack.com/query/v4/docs/guides/queries
 * @returns
 */
const useGetListMovie = (props: IPayloadGetListMovie) => {
  // perlu kasih nama prefix namaModule di depan nya untuk yg sifat nya list karena biar gak bentrok
  const data: any = useQuery(
    ['get-list-movie'],
    () => {
      return services.homeGetListMovie();
    },
    {
      enabled: props?.isFetch,
    },
  );
  return data;
};

export default useGetListMovie;
