import useGetDetailMovie from './useGetDetailMovie';
import useGetListMovie from './useGetListMovie';

export {
  useGetDetailMovie,
  useGetListMovie,
};
