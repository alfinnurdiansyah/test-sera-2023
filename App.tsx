import React, { useMemo } from 'react';
import {persistor, store} from '@configs/store';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import Navigation from '@navigation/index';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
interface Props {}
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: 2,
    },
  },
});
const App: React.FC<Props> = () => {
  const RenderMain = useMemo(() => {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <SafeAreaProvider>
            <QueryClientProvider client={queryClient}>
              <Navigation />
            </QueryClientProvider>
          </SafeAreaProvider>
        </PersistGate>
      </Provider>
    );
  }, []);

  return RenderMain;
};

export default App;
